import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';

import { MeuPrimeiroComponent } from './primeiro/meu-primeiro.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { LifeCycleComponent} from './ciclo/life-cycle.component';

import { CursosModule } from './cursos/cursos.module';
import { DataBindingModule } from './data-binding/data-binding.module';



@NgModule({
  imports: [ 
    BrowserModule, 
    FormsModule, 
    CursosModule,
    DataBindingModule,
   ],
  declarations: [ 
    AppComponent, 
    MeuPrimeiroComponent, 
    //DataBindingComponent, 
    LifeCycleComponent 
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }