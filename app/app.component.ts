import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        <h1>Angular 2 Boilerplate</h1>
        <p>Hello World!</p>
        
        <meu-primeiro-componente></meu-primeiro-componente>
        <cursos-lista></cursos-lista>
        
        <exemplo-data-binding></exemplo-data-binding>
        
        <lifecycle [valorInicial]="valorInicial" *ngIf="deletarConteudo != true"></lifecycle>
        <button (click)="valorInicial = 20">Mudar o Valor </button>
        <button (click)="deletarConteudo = true">Deletar tag de life cycle </button>
         
    `
})
export class AppComponent { 
    
    deletarConteudo : boolean = false;
    valorInicial : number = 15;
}
