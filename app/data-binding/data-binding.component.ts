import { Component } from '@angular/core';

//import { InputPropertyComponent } from './input-property.component';
//import { OutputPropertyComponent } from './output-property.component';

@Component({
    moduleId: module.id,
    selector: 'exemplo-data-binding',
    templateUrl: 'data-binding.component.html'    ,
    /*styles: [
        `
            .highlight {
                background-color: yellow;
                font-weight: bold;
            }
        `
    ]*/
    styleUrls: ['data-binding.component.css']//,
    //directives: [InputPropertyComponent, OutputPropertyComponent]
})
export class DataBindingComponent {
    constructor() { }

    url = 'http://localhost:3000/';
    urlimg = 'http://docs.nativescript.org/angular/img/cli-getting-started/angular/chapter0/Angular_logo.png';
    altimg = "Angular 2";
    
    conteudoAtual : string = '';
    conteudoSalvo : string = '';
    isMouseOver : boolean = false;

    nome : string = '';

    pessoa = {nome: '', idade: 18};

    nomeCurso : string = 'Curso Angular 2';

    valorInicial : number = 10;

    getValor(){
        return 1;
    }

    onClick(){
        alert('Botao Clicado');
    }

    onKeyup(event:KeyboardEvent){
        console.log(event);
        this.conteudoAtual = (<HTMLInputElement>event.target).value;
    }

    onSave(valor : string){
        this.conteudoSalvo = valor;
    }

    onMouseSpan(){
        this.isMouseOver = !this.isMouseOver;
    }

    onValorMudou(event){
        alert(event.novoValor);
    }
}